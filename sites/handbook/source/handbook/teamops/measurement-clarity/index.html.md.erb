---
layout: handbook-page-toc
title: "TeamOps - Measurement Clarity"
description: TeamOps - Measurement Clarity
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc}

- TOC
{:toc}

![GitLab TeamOps teamwork illustration](/handbook/teamops/images/teamops-illustration_teamwork_purple.png)
{: .shadow.medium.center}

This page is about one of the four Guiding Principles of TeamOps: Measurement Clarity. To learn more about the other three Principles, return to the main TeamOps page for a [complete overview of TeamOps](/teamops/), or enroll in the free [TeamOps course](https://levelup.gitlab.com/learn/course/teamops). 
{: .alert .alert-info}


# Measurement clarity 

_Outdated workforce supervision tactics often trigger bias and presenteeism, so results must be uniquely recorded, managed, and supported for the accurate tracking and evaluation of productivity._

Like Peter Drucker famously said, “If you can't measure it, you can't manage it.” But for many generations of business, the primary success metric was physical presence – an employee needed to be in a certain building in order to be at work. 

TeamOps supports the revolution that work is a verb, not a noun. Therefore, teams should rely on measuring productivity, value, and results without depending on physical supervision. Reprioritizing what is measured, how it’s measured, and when it’s measurement enables a higher frequency of success analysis, higher accountability to objectives, lower workforce discrimination, and a wider reach of company communication.

Action tenets of strengthening measurement clarity, including real-world examples of each, are below.


## No-matrix organization

You should only have **one manager**. Conventional management philosophies may focus on minimizing the shortcomings of matrix organizations (or, "dotted line" reporting structures) but refuse to eliminate them. TeamOps asserts that a [no-matrix organization](/handbook/leadership/no-matrix-organization/) is not only feasible, but *essential* to executing decisions. By ensuring that each individual reports to exactly one other individual, contributions have a clear path to receiving feedback. 

<details>
   <summary markdown="span">Example of no-matrix organization</summary>

   **Example 1:** Every GitLab team member has exactly one manager

   In an organization powered by TeamOps, a team member specializing in sales accounting (in this example, the team member's title is `FP&A Analyst, Sales Finance`) reports to **exactly one person** with domain knowledge (with the title `Sr. Director, Sales Finance`). This *one* manager understands their report's day-to-day tasks, has served in that role prior, is positioned to share a single set of goals, and is suited to coach the team member through a mutually understood career path. The team member does not "dual report" to Finance and Sales. 
   
   Rather than relying on reporting structure to ensure collaboration, [collaboration is installed at a foundational, organization-wide level](/handbook/values/#collaboration). Each function is expected to honor the associated operating principles. Whenever there is a need to work on a specific, high-level, cross functional business problem, assemble a [working group](/company/team/structure/working-groups/) only for the duration of time required to execute.
</details>


## Measure results, not hours

The goal of every operational model is to optimize the efficiency of producing results, but conventional teams make a critical error when they conflate “efficiency” to mean “speed.” This means time becomes the highest priority of the team, and working hours become a primary success metric for the organization. 

In organizations powered by TeamOps, team members understand that the root of “productivity” is “to produce,” and therefore focus on [executing business results](/handbook/values/#measure-results-not-hours), rather than executing on [presenteeism](https://language.work/research/killing-time-at-work/). All success measurements should be based on outputs, not inputs. 

Note that outputs aren’t just tangible deliverables. Results include any value to the shared reality that a team member contributes: helping a teammate, satisfying a customer, shipping code, brainstorming a new idea, writing a revision, or researching a competitor. All quantifiable reports, messages, insights, or submissions are evidence of time well spent.

<details>
   <summary markdown="span">Example of measure results, not hours</summary>

   **Example 1:** [Measuring impact of GitLab's 10 year campaign](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507)
   
   A cross-functional effort was required to produce the `10 Years of GitLab` integrated marketing campaign and [associated website](/ten/). A GitLab issue was established to explicitly define [elements to be tracked and measured](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507) in order to provide an impact report. By focusing on results over hours spent (or if a given team member was online at a certain time, or in a certain office), everyone involved in the project can focus energy on execution. 
</details>


## Transparent measurements

Conventional management philosophies glorify metrics, which is a nonspecific term that often lacks connection to objectives, mission, values, workflows, strategy, or a shared reality. TeamOps prefers [Key Performance Indicators (KPIs)](/company/kpis/), which are smaller increments linked to [Objectives and Key Results](/company/okrs/) (OKRs) that, well, _indicate performance_ and offer greater context on daily operations and the relevance of ongoing productivity to a function or the entire company. 

While smaller than OKRs, KPIs are not dependent on them. In fact, the two should be symbiotic in nature – informing and influencing each other for greater operational visibility, tracking accuracy, and team empowerment. If you're not creating OKRs to improve KPIs, then you're either missing KPIs or you have the wrong OKRs. 

Crucially, KPIs for each function are [transparently shared](/handbook/values/#findability) across the organization. This enables everyone to contribute by creating visibility between departments.

<details>
   <summary markdown="span">Example of transparent measurements</summary>

   **Example 1:** [Chief Executive Officer OKR and KPIs](/company/okrs/fy23-q3/)

   In Q3-FY23, a CEO OKR is [Improve user and wider-community engagement](/company/okrs/fy23-q3/). This is the *initiative* to improve a series of KPIs, a subset of which are documented below: 
   1. Evolve the resident contributor strategy by conducting 5 customer conversations with current “resident contributors” in seat
   2. Certify 1,000 team members and 10,000 wider-community members in TeamOps 
   3. Enhance Corporate Processes and Successful Corporate Development Integration & Prospecting 

   These are documented in a tool (GitLab uses Ally) that is accessible to the entire organization. Critically, any team member can see any other functions OKRs and KPIs for the quarter, reinforcing the [value of transparency](/handbook/values/#transparency).
</details>


## Iteration

Executing on a decision should not be binary, nor a one-time event. TeamOps reframes execution as an ongoing series of [iterations](/handbook/values/#iteration) – small, compounding results – each one worthy of celebration. This encourages [smaller steps](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change), which are more amenable to feedback, course correction, and easier to deliver. By breaking decisions down into manageable components, results are more feasible.

In conventional organizations, there's often inherent pressure to present a complete and polished project, document, or plan. This expectation slows progress and expends valuable time that could be used to exchange multiple rounds of feedback on smaller changes.

A key aspect of TeamOps is incorporating iteration into every process and decision with a [low level of shame](/handbook/values/#low-level-of-shame). This means doing the smallest viable and valuable thing, and getting it out quickly for feedback. Despite the initial discomfort that comes from sharing the [minimal viable change (MVC)](/handbook/values/#minimal-viable-change-mvc), iteration enables faster execution, a shorter feedback loop, and the ability to course-correct sooner.

This philosophy mirrors the GitLab product from a cycle-time standpoint. GitLab is built to reduce the time between making a decision and getting the result to market. Iteration enables cycle time reduction to be applied in day-to-day decision making.

<details>
   <summary markdown="span">Example of iteration enables execution</summary>

   **Example 1:** [Adding an MVC GitLab Citation Index to GitLab for Education homepage](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69665/)

   In a conventional organization, a revised homepage could be seen as binary. It's either complete, or not. [This merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69665/) details a minimum viable change (MVC) to add one element to the [GitLab for Education](/solutions/education/) homepage. In the comment thread, GitLab team members agree that this iteration moves the page one step closer to an ideal state. By embracing and celebrating each iteration *as* execution, it enables team members to accelerate execution on other projects rather than being stuck on a prior project. 
</details>


## Prioritize due dates over scope

Although time is not a success measurement, TeamOps requires due dates. This is not a means to create unnecessary rigidity or measure duration of contributions, but to force mechanisms that enable teams to execute on decisions and require accountability.

A TeamOps organization will always [set a due date](/handbook/values/#set-a-due-date), and if necessary, will cut scope to meet the due date rather than postpone the date. This forces the time to think iteratively, by saving some of the scope for a future objective (or future execution), while limiting the loss of momentum.

<details>
   <summary markdown="span">Example of prioritize due dates over scope</summary>

   **Example 1:** [Maintaining a monthly release cadence for 10+ years](/releases/)

   As of April 30, 2022, GitLab has shipped a monthly product release for [127 consecutive quarters](https://ir.gitlab.com/news-releases/news-release-details/gitlab-reports-first-quarter-fiscal-year-2023-financial-results). That's over 10 years! A decade of [consistent execution](/blog/2018/11/21/why-gitlab-uses-a-monthly-release-cycle/) is made possible by [cutting scope](/handbook/values/#set-a-due-date) instead of pushing ship dates. 
</details>


## Key Review meetings

When and where are your team members able to make their contributions, and receive contributions from others? In addition to standard communication channels, recurring opportunities dedicated exclusively to updates can help optimize awareness, questions, and feedback about an ongoing project. 

These [Key Review Meetings](/handbook/key-review/) present a functional group to stay updated on and discuss essential success measurements, such as: OKRs, KPIs, how the team is trending toward achieving goals, blocked tasks, new assignments, workstream changes, etc. 

In conventional organizations, this is apt to be a more informal conversation between a department head and their manager. By broadening the audience of attendees for a Key Review Meeting to include the Chief Executive Officer (CEO), Chief Financial Officer (CFO), the function head, stable counterparts, and (optionally) all other executives and their direct reports, we widen the pool of people who can contribute feedback, insights, and advice. It forces the presenting department to be more mindful of execution, consider areas where they are falling short, and gather input for potential iterations toward progress.  

<details>
   <summary markdown="span">Example of key review meetings</summary>

   **Example 1:** [GitLab User Experience (UX) department Key Review meeting](https://youtu.be/54LAX8UFU9s)

   GitLab User Experience (UX) department [livestreamed a Key Review meeting on GitLab Unfiltered](https://youtu.be/54LAX8UFU9s). A distinct element of these meetings is that [no presentations are allowed](/handbook/communication/#no-presenting-in-meetings). For context, each attendee was able to view a presentation prepared ahead of time, with a shared Google Doc agenda used to maintain an orderly and inclusive flow of questions and conversation. You'll notice that executives and their direct reports provide questions and suggestions throughout. There's a distinct conversation on usability beginning at the 13:51 mark where leads from various functions [contribute to improved execution on a 25-second lag recognized in the product](https://youtu.be/54LAX8UFU9s?t=831). 
</details>


## Stable counterparts

Often, decision velocity is easily maintained inside of team, but then slows when information is shared outside of it. It is essential that various contributors throughout the organization can be looped into a decision for review, collaboration, and feedback with minimal impact.

In a [stable counterparts model](/blog/2018/10/16/an-ode-to-stable-counterparts/) for enabling cross-functional execution, every functional team (e.g. Support) works with the same team members from a different functional team (e.g. Development). As a member of one function, you always know who your partner in another function will be. Stable counterparts are an intentionally chosen structure designed to execute on decisions. They foster collaboration *across functions* by giving people stable counterparts for other functions they need to work with to execute decisions. This enables more social trust and familiarity, which [speeds up decision making](/handbook/teamops/fast-decisions/), facilitates [stronger communication flows](/handbook/teamops/informed-decisions/), and reduces the risk of conflicts. 

Stable counterparts enhance cross-functional execution without the potential conflicts that happen in a matrix organization. Like a project or decision should only have one Directly Responsible Individual (DRI), an individual should only have one manager. Conventional management philosophies may focus on minimizing the shortcomings of matrix organizations (or, "dotted line" reporting structures) but refuse to eliminate them. TeamOps asserts that a [no-matrix organization](/handbook/leadership/no-matrix-organization/) is not only feasible, but essential to executing decisions. By ensuring that each individual reports to exactly one other individual, feedback and approval processes are as streamlined as possible.  

<details>
   <summary markdown="span">Example of stable counterparts</summary>
   
   **Example 1:** [Technical Support stable counterparts](/handbook/support/support-stable-counterparts.html)

   Support team members are [assigned a permanent contact](/handbook/support/support-stable-counterparts.html) for a GitLab team member within another function in the company. The ability to build long-term relationships is the foundational benefit of having stable counterparts. Repeated interactions help us understand personal workflows and communication styles, so we know how to most effectively execute decisions with our counterparts.
</details>


---

Return to the [TeamOps homepage](/teamops/). 
